package com.company;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {

        final String bookStr = readFile("Стивен Кинг - Доктор Сон (ukr) - Copy - Copy.fb2", Charset.defaultCharset());

        String newBookStr = bookStr;
        int foundPos = 0;
        while ((foundPos = bookStr.indexOf("<a l:href=\"#n_", foundPos)) != -1) {
            final int start = foundPos;
            final int end = bookStr.indexOf("</a>", start) + 4;
            final String footnote = bookStr.substring(start, end);
            final String number = footnote.substring(footnote.indexOf("[") + 1, footnote.indexOf("]"));
            final String newFootnote = footnote.replace("#n_\"", "#n_" + number + "\"");
            newBookStr = newBookStr.replace(footnote, newFootnote);
            foundPos++;
        }

        try (PrintStream ps = new PrintStream("filename.fb2")) {
            ps.println(newBookStr);
        }
    }

    static String readFile(String path, Charset encoding) throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
